"""
.. module:: wfmetadataselect
   :platform: Linux
   :synopsis: Wfmetadataselect Webservice for EIDA

.. moduleauthor:: Andres Heinloo <andres@gfz-potsdam.de>, GEOFON, GFZ Potsdam
"""

import os
import re
import math
import pymongo
import json
import datetime
import dateutil.parser
import ConfigParser
import logs
from wsgicomm import *

VERSION = "0.1 (2014.306)"

class JsonOutput(object):
    def __init__(self):
        pass

    def __seg(self, seg):
        return {
            'min': seg['min'],
            'max': seg['max'],
            'rms': seg['rms'],
            'std_dev': seg['stdv'],
            'number_of_samples': seg['n_sam'],
            'segment_length': seg['s_len'],
            'start_time': seg['ts'].isoformat() + 'Z',
            'end_time': seg['te'].isoformat() + 'Z',
            'id': str(seg['_id'])
        }

    def output(self, rec, seg_iter):
        continuous_segments = None

        if seg_iter is not None:
            continuous_segments = []

            for seg in seg_iter:
                continuous_segments.append(self.__seg(seg))

        return {
            'net': rec['net'],
            'sta': rec['sta'],
            'locId': rec['loc'],
            'ch': rec['cha'],
            'min': rec['min'],
            'max': rec['max'],
            'rms': rec['rms'],
            'std_dev': rec['stdv'],
            'avail': rec['avail'],
            'gaps': rec['n_gaps'],
            'over': rec['n_over'],
            'sample_rate': rec['s_rate'],
            'data_quality': rec['d_qlt'],
            'record_length': rec['r_len'],
            'start_time': rec['ts'].isoformat() + 'Z',
            'end_time': rec['te'].isoformat() + 'Z',
            'id': str(rec['_id']),
            'continuous_segments': continuous_segments
        }

class PostOutput(object):
    def __init__(self):
        pass

    def output(self, rec, seg_iter):
        # take start-end times given by user if applicable
        starttime = max(rec['starttime'], rec['ts']) if 'starttime' in rec else rec['ts']
        endtime = min(rec['endtime'], rec['te']) if 'endtime' in rec else rec['te']

        return str("%s %s %s %s %s %s\n" % (
            rec['net'], rec['sta'], rec['loc'] if rec['loc'] else '--', rec['cha'],
            starttime.isoformat(), endtime.isoformat()))

# The "Collector" reduces multiple segments into one. Might try
# to use mongodb map_reduce instead.

class Collector(object):
    def __init__(self, param, starttime, endtime, c_segments):
        self.__param = param
        self.__starttime = starttime
        self.__endtime = endtime
        self.__c_segments = c_segments
        self.__stations = {}
        self.__filtered = set()

    def __in_range(self, what, rec):
        low = self.__param.get('low' + what)
        up = self.__param.get('up' + what)

        if low and rec[what] < low:
            return False

        if up and rec[what] > up:
            return False

        return True

    def __pre_check(self, rec):
        noover = self.__param.get('noover')
        nogaps = self.__param.get('nogaps')
        dq = self.__param.get('quality')

        if dq and rec['d_qlt'] != dq: # FIXME
            return False

        for what in ('max', 'min'):
            if not self.__in_range(what, rec):
                return False

        if ((noover and rec['n_over']) or (nogaps and rec['n_gaps'])) and \
            ((rec['ts'] >= self.__starttime and rec['te'] <= self.__endtime) or \
                not self.__c_segments.find_one({ '$and': [{ 'stream_id': rec['_id'] },
                                                          { 'ts': { '$lte': max(self.__starttime, rec['ts']) }},
                                                          { 'te': { '$gte': min(self.__endtime, rec['te']) }}]})):
            return False

        return True

    def __post_check(self, rec):
        nogaps = self.__param.get('nogaps')

        # if 'nogaps' is requested, ensure that the whole timespan is available
        # (eg., no 'gaps' in the beginning or end of data are allowed)
        # TODO: use an extra parameter for this
        if nogaps and (rec['ts'] > self.__starttime or rec['te'] < self.__endtime):
            return False

        for what in ('rms', 'stdv'):
            if not self.__in_range(what, rec):
                return False

        return True

    def update(self, rec):
        ns = (rec['net'], rec['sta'])
        lc = (rec['loc'], rec['cha'])

        if (ns, lc) in self.__filtered:
            return

        st = self.__stations.setdefault(ns, {})

        # TODO: channel grouping? If one of (BHZ, BHN, BHE) does not pass the
        # quality criteria, should all 3 be removed?

        if not self.__pre_check(rec):
            st.pop(lc, None)
            self.__filtered.add((ns, lc))
            return

        try:
            ch = st[lc]

        except KeyError:
            ch = rec.copy()
            ch['starttime'] = self.__starttime
            ch['endtime'] = self.__endtime
            st[lc] = ch
            return

        if rec['ts'] < ch['ts']:
            ch['ts'] = rec['ts']

        if rec['te'] > ch['te']:
            ch['te'] = rec['te']

        if rec['min'] < ch['min']:
            ch['min'] = rec['min']

        if rec['max'] > ch['max']:
            ch['max'] = rec['max']

        ch['rms'] = math.sqrt((ch['rms']**2 * ch['n_sam'] + rec['rms']**2 * rec['n_sam']) / \
            (ch['n_sam'] + rec['n_sam']))

        # this is not strictly correct if offset (mean) changes
        ch['stdv'] = math.sqrt((ch['stdv']**2 * ch['n_sam'] + rec['stdv']**2 * rec['n_sam']) / \
            (ch['n_sam'] + rec['n_sam']))

        n_sam_expect = 100.0 * (ch['n_sam'] / ch['avail'] + rec['n_sam'] / rec['avail'])
        ch['avail'] = 100.0 * (ch['n_sam'] + rec['n_sam']) / n_sam_expect

        for what in ('n_sam', 'n_over', 'n_gaps', 'over_len', 'g_len'):
            ch[what] += rec[what]

    def __filter_channels(self, st):
        chlist = filter(self.__post_check, st.itervalues())
        prefsamprate = self.__param.get('prefsamprate')

        if prefsamprate:
            sr = 1000000

            for ch in chlist:
                if abs(prefsamprate - ch['s_rate']) < abs(prefsamprate - sr):
                    sr = ch['s_rate']

            chlist[:] = filter(lambda ch: ch['s_rate'] * 0.99 <= sr <= ch['s_rate'] * 1.01, chlist)

        return chlist

    def __iter__(self):
        return iter(sum(map(self.__filter_channels, self.__stations.itervalues()), []))

class RequestIterator(object):
    def __init__(self, twlist, params):
        self.__twiter = iter(twlist)
        self.__params = params

    def __iter__(self):
        return self

    def __regex(self, pattern):
        return '^' + pattern.replace('*', '.*').replace('?', '.') + '$'

    def __nslc(self, what, item):
        q = []

        for code in item.split(','):
            if what == 'loc' and code == '--':
                q.append({ what: '' })

            elif code != '*':
                if '*' in code or '?' in code:
                    q.append({ what: { '$regex': self.__regex(code) }})

                else:
                    q.append({ what: code })

        if len(q) > 1:
            return [ { '$or': q } ]

        return q

    def __low_up(self, what):
        q = []

        try:
            q.append({ what: { '$gte': self.__params['low' + what] }})

        except KeyError:
            pass

        try:
            q.append({ what: { '$lte': self.__params['up' + what] }})

        except KeyError:
            pass

        return q

    def __mongo_query(self, net, sta, loc, cha, ts, te):
        q = []

        q.extend(self.__nslc('net', net))
        q.extend(self.__nslc('sta', sta))
        q.extend(self.__nslc('loc', loc))
        q.extend(self.__nslc('cha', cha))
        q.append({ 'ts': { '$lte': te }})
        q.append({ 'te': { '$gt': ts }})

        # TODO: optimized query for collapsed POST output
        if not self.__params.get('collapsed'):
            for what in ('max', 'min', 'rms', 'stdv'):
                q.extend(self.__low_up(what))

            try:
                q.append({ 'd_qlt': self.__params['quality']})

            except KeyError:
                pass

            if self.__params.get('noover'):
                q.append({ 'n_over': 0 })

            if self.__params.get('nogaps'):
                q.append({ 'n_gaps': 0 })

        return ({ "$and": q }, ts, te)

    def next(self):
        return self.__mongo_query(*self.__twiter.next())

class ExpandedResultIterator(object):
    def __init__(self, wfrepo, req_iter, fmt, param):
        self.__wfrepo = wfrepo
        self.__req_iter = req_iter
        self.__fmt = fmt
        self.__param = param
        self.__ts = None
        self.__te = None
        self.__cursor = None

    def __iter__(self):
        return self

    def next(self):
        while True:
            if not self.__cursor:
                q, self.__ts, self.__te = self.__req_iter.next()
                self.__cursor = self.__wfrepo.daily_streams.find(q)

            try:
                rec = self.__cursor.next()

                if self.__param.get('extended'):
                    q = [{ 'stream_id': rec['_id'] },
                         { 'ts': { '$lte': self.__te }},
                         { 'te': { '$gt': self.__ts }}]

                    minimumlength = self.__param.get('minimumlength')

                    if minimumlength is not None:
                        q.append({ 's_len': { '$gte': minimumlength }})

                    q = { "$and": q }

                    if self.__param.get('longestonly'):
                        seg = self.__wfrepo.c_segments.find_one(q, sort=[("segment_length", -1)])
                        seg_iter = [ seg ] if seg else []

                    else:
                        seg_iter = self.__wfrepo.c_segments.find(q)

                else:
                    seg_iter = None

                return self.__fmt.output(rec, seg_iter)

            except StopIteration:
                self.__cursor = None

class CollapsedResultIterator(object):
    def __init__(self, wfrepo, req_iter, fmt, param):
        self.__wfrepo = wfrepo
        self.__req_iter = req_iter
        self.__fmt = fmt
        self.__param = param
        self.__cursor = None
        self.__collector = None
        self.__coll_iter = None

    def __iter__(self):
        return self

    def next(self):
        while True:
            if not self.__coll_iter:
                if not self.__cursor:
                    q, ts, te = self.__req_iter.next()
                    self.__cursor = self.__wfrepo.daily_streams.find(q)
                    self.__collector = Collector(self.__param, ts, te, self.__wfrepo.c_segments)

                loop = 0

                while True:
                    loop += 1

                    try:
                        self.__collector.update(self.__cursor.next())

                    except StopIteration:
                        self.__cursor = None
                        break

                    # give control shortly back to Apache after processing 100000 records
                    if loop >= 100000:
                        logs.debug("100000 records processed")
                        return None

                self.__coll_iter = iter(self.__collector)

            try:
                return self.__fmt.output(self.__coll_iter.next(), None)

            except StopIteration:
                self.__collector = None
                self.__coll_iter = None

class WI_Module(object):
    def __init__(self, wi):
        wi.registerAction("/wfmetadataselect/1/version", self.version)
        wi.registerAction("/wfmetadataselect/1/application.wadl", self.application_wadl)
        wi.registerAction("/wfmetadataselect/1/query", self.query)

        self.wadl = os.path.join(wi.server_folder, "wadl", "wfmetadataselect.wadl")

        try:
            mongoaddr = wi.config.get("MongoDB", "address")

        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            logs.warning("MongoDB address is not defined in config file")
            mongoaddr = "mongodb://localhost:27017/"

        try:
            repo = wi.config.get("MongoDB", "repo")

        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            logs.warning("MongoDB repository name is not defined in config file")
            repo = "wfrepo"

        db = pymongo.MongoClient(mongoaddr)
        self.wfrepo = db[repo]

    def version(self, environ, form):
        return WISimpleResponse(VERSION)

    def application_wadl(self, environ, form):
        try:
            with open(self.wadl) as fp:
                return WISimpleResponse(fp.read(), content_type="text/xml; charset=UTF-8")

        except IOError as e:
            logs.error(str(e))
            raise WIInternalError("")

    def _nslc(self, form, name):
        if name in form:
            code = form.getfirst(name).upper()

        elif name[:3] in form:
            code = form.getfirst(name[:3]).upper()

        else:
            return '*'

        if not re.match('^[A-Z0-9\*\?\-,]+$', code):
            raise WIClientError("Error while converting %s parameter" % name)

        return code

    def _time(self, form, name):
        if name in form:
            timestring = form.getfirst(name).upper()

        elif name[:-4] in form:
            timestring = form.getfirst(name[:-4]).upper()

        else:
            raise WIClientError("The field %s (%s) is mandatory" % (name, name[:-4]))

        try:
            return dateutil.parser.parse(timestring)

        except ValueError:
            raise WIClientError('Error while converting %s parameter' % name)

    def query(self, environ, form):
        param = {}

        # string parameters
        for k in ('format', 'quality'):
            if k in form:
                param[k] = form.getfirst(k)

        # boolean parameters
        for k in ('collapsed', 'extended', 'longestonly', 'noover', 'nogaps'):
            if k in form:
                param[k] = form.getfirst(k).lower() == 'true'

        # float parameters
        for k in ('minimumlength', 'lowmax', 'upmax', 'lowmin', 'upmin',
            'lowrms', 'uprms', 'lowstdv', 'upstdv', 'prefsamprate'):
            if k in form:
                try:
                    param[k] = float(form.getfirst(k))

                except ValueError:
                    raise WIClientError('Parameter %s has invalid value')

        if form.body is None:
            net = self._nslc(form, 'network')
            sta = self._nslc(form, 'station')
            loc = self._nslc(form, 'location')
            cha = self._nslc(form, 'channel')
            start = self._time(form, 'starttime')
            endt = self._time(form, 'endtime')
            twlist = [(net, sta, loc, cha, start, endt)]

            # special cases (ugh!)
            qs = environ['QUERY_STRING'].split('&')

            if 'extended' in qs:
                param['extended'] = True

            if 'noover' in qs:
                param['noover'] = True

            if 'nogaps' in qs:
                param['nogaps'] = True

        else:
            twlist = form.body

        req_iter = RequestIterator(twlist, param)

        if param.get('collapsed'):
            param['extended'] = False
            ResultIterator = CollapsedResultIterator

        else:
            ResultIterator = ExpandedResultIterator

        if param.get('format') == 'post':
            param['extended'] = False
            res_iter = ResultIterator(self.wfrepo, req_iter, PostOutput(), param)
            res_iter.filename = "wfmetadataselect_%s.post" % (datetime.datetime.utcnow().isoformat())
            res_iter.content_type = "text/plain"
            return WIDynamicFileResponse(res_iter)

        else:
            res_iter = ResultIterator(self.wfrepo, req_iter, JsonOutput(), param)
            res_iter.filename = "wfmetadataselect_%s.json" % (datetime.datetime.utcnow().isoformat())
            return WIDynamicJSONResponse(res_iter)

