#!/usr/bin/env python
#
# Resources to communicate via a WSGI module
#
# Begun by Javier Quinteros, GEOFON team, June 2013
# <javier@gfz-potsdam.de>
#
# ----------------------------------------------------------------------


"""Functions and resources to communicate via a WSGI module

(c) 2013 GEOFON, GFZ Potsdam

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any later
version. For more information, see http://www.gnu.org/

"""

import json
import logs

##################################################################
#
# Exceptions to be caught (usually) by the application handler
#
##################################################################


class PlsRedirect(Exception):
    """Exception to signal that the web client must be redirected to a URL.

    The constructor of the class receives a string, which is the
    URL where the web browser is going to be redirected.

    Begun by Javier Quinteros <javier@gfz-potsdam.de>, GEOFON team, June 2013

    """

    def __init__(self, url):
        self.url = url

    def __str__(self):
        return repr(self.url)


class WIError(Exception):
    """Exception to signal that an error occurred while doing something,
    that the web client should see.

    Inputs:
      status     - string, like "200 Good", "400 Bad"
      body       - string, plain text content to display to the client

    """

    def __init__(self, status, body):
        self.status = status
        self.body = body

    def __str__(self):
        return repr(self.status) + ': ' + repr(self.body)


class WIContentError(WIError):
    def __init__(self, body):
        WIError.__init__(self, "204 No Content", body)


class WIClientError(WIError):
    def __init__(self, body):
        WIError.__init__(self, "400 Bad Request", body)


class WIInternalError(WIError):
    def __init__(self, body):
        WIError.__init__(self, "500 Internal Server Error", body)


class WIServiceError(WIError):
    def __init__(self, body):
        WIError.__init__(self, "503 Service Unavailable", body)


class WIResponse(object):
    def __init__(self, func, body, **kwargs):
        self.func = lambda status, body, start_response: \
            func(status, body, start_response, **kwargs)
        self.body = body


class WISimpleResponse(WIResponse):
    def __init__(self, body, content_type='text/plain', extra_headers=None):
        WIResponse.__init__(self, _send_plain_response, body,
            content_type=content_type, extra_headers=extra_headers)


class WIFileResponse(WIResponse):
    def __init__(self, body):
        WIResponse.__init__(self, _send_file_response, body)


class WIDynamicFileResponse(WIResponse):
    def __init__(self, body):
        WIResponse.__init__(self, _send_dynamicfile_response, body)


class WIDynamicJSONResponse(WIResponse):
    def __init__(self, body):
        WIResponse.__init__(self, _send_dynamic_json_response, body)


##################################################################
#
# Functions to send a response to the client
#
##################################################################

def _redirect_page(url, start_response):
    """Tells the web client through the WSGI module to redirect to an URL.

    Begun by Javier Quinteros <javier@gfz-potsdam.de>, GEOFON team, June 2013

    """

    response_headers = [('Location', url)]
    start_response('301 Moved Permanently', response_headers)
    return ''


def _send_plain_response(status, body, start_response, content_type='text/plain', extra_headers=None):
    """Sends a plain response in WSGI style.

    Begun by Javier Quinteros <javier@gfz-potsdam.de>, GEOFON team, June 2013

    """

    response_headers = [('Content-Type', content_type),
                        ('Content-Length', str(len(body)))]

    if extra_headers:
        response_headers.append(extra_headers)

    start_response(status, response_headers)
    return [body]


def _send_file_response(status, body, start_response):
    """Sends a file or similar object.

    Caller must set the filename, size and content_type attributes of body.

    """
    response_headers = [('Content-Type', body.content_type),
                        ('Content-Length', str(body.size)),
                        ('Content-Disposition', 'attachment; filename=%s' %
                         (body.filename))]
    start_response(status, response_headers)
    return body


def _send_dynamicfile_response(status, body, start_response):
    """Sends a file or similar object.

    Caller must set the filename and content_type attributes of body.

    """

    # Cycle through the iterator in order to retrieve one chunck at a time
    loop = 0
    for data in body:
        if loop == 0:
            # The first thing to do is to send the headers.
            # This needs to be done here so that we are sure that there is
            # ACTUALLY data to send

            # Content-length cannot be set because the file size is unknown
            response_headers = [('Content-Type', body.content_type),
                                ('Content-Disposition', 'attachment; filename=%s' %
                                 (body.filename))]
            start_response(status, response_headers)

        # Increment the loop count
        loop += 1
        # and send data
        if data is None:
             logs.debug("None received")
             yield ''
             continue

        yield data

    if loop == 0:
        status = '204 No Content'
        response_headers = []
        start_response(status, response_headers)
        logs.debug('204 sent')

def _send_dynamic_json_response(status, body, start_response):
    """Sends a file or similar object.

    Caller must set the filename attribute of body.

    """
    firstdata = True

    # Cycle through the iterator in order to retrieve one chunck at a time
    loop = 0
    for data in body:
        if loop == 0:
            # The first thing to do is to send the headers.
            # This needs to be done here so that we are sure that there is
            # ACTUALLY data to send

            # Content-length cannot be set because the file size is unknown
            response_headers = [('Content-Type', 'application/json'),
                                ('Content-Disposition', 'attachment; filename=%s' %
                                 (body.filename))]
            start_response(status, response_headers)

            # Increment the loop count
            loop += 1
            # and send data
            if data is None:
                logs.debug("None received")
                yield ''
                continue

            yield '[' + json.dumps(data, indent=4)
            firstdata = False
            continue

        # Increment the loop count
        loop += 1
        # and send data
        if data is None:
             logs.debug("None received")
             yield ''
             continue

        if firstdata:
            yield '[' + json.dumps(data, indent=4)
            firstdata = False
            continue

        yield ',\n' + json.dumps(data, indent=4)

    if loop == 0:
        status = '204 No Content'
        response_headers = []
        start_response(status, response_headers)
        logs.debug('204 sent')

    elif firstdata:
        yield '[]'

    else:
        yield ']'

