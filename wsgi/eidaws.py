#!/usr/bin/env python

"""EIDA Webservices

(C) EIDA 2014

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any later
version. For more information, see http://www.gnu.org/

"""

import os
import sys
import glob
import imp
import io
import re
import cgi
import datetime
import dateutil.parser
import ConfigParser
import logs
from wsgicomm import WIError, WIContentError, WIResponse, PlsRedirect, _send_plain_response

VERSION = "0.1 (2014.306)"

# Maximum size of POST data in bytes
cgi.maxlen = 1000000

class WebInterface(object):
    def __init__(self, appName):
        verbosity = 3

        def make_logger(prefix, level):
            def logger(msg):
                if verbosity >= level:
                    print >>sys.stderr, prefix, msg
                    sys.stderr.flush()

            return logger

        logs.debug = make_logger("[debug]", 4)
        logs.info = make_logger("[info]", 3)
        logs.notice = make_logger("[notice]", 2)
        logs.warning = make_logger("[warning]", 2)
        logs.error = make_logger("[error]", 1)

        logs.notice("Starting EIDA Webservices v%s" % VERSION)

        self.server_folder = os.path.normpath(os.path.join(os.path.dirname(__file__), os.path.pardir))
        self.config = ConfigParser.RawConfigParser()

        try:
            self.config.read(os.path.join(self.server_folder, "eidaws.cfg"))
            verbosity = int(self.config.get('Service', 'verbosity'))

        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            pass

        except (ConfigParser.Error, ValueError) as e:
            logs.error(str(e))

        self.__action_table = {}
        self.__modules = {}

        # Add inventory cache here, to be accessible to all modules
        #inventory = os.path.join(self.server_folder, "data", "Arclink-inventory.xml")
        #self.ic = InventoryCache(inventory)

        # Load all modules in given directory.
        # Modules must contain a class WI_Module, whose __init__() takes
        # WebInterface object (our self) as an argument and calls our
        # addAction().

        for f in glob.glob(os.path.join(self.server_folder, "wsgi", "services", "*.py")):
            self.__load_module(f)

    def __load_module(self, path):
        modname = os.path.splitext(os.path.basename(path))[0].replace('.', '_')

        if modname in self.__modules:
            logs.error("'%s' is already loaded!" % modname)
            return

        try:
            mod = imp.load_source('__wi_' + modname, path)

        except:
            logs.error("error loading '%s'" % modname)
            logs.print_exc()
            return

        self.__modules[modname] = mod.WI_Module(self)

    def registerAction(self, name, func):
        self.__action_table[name] = func

    def getAction(self, name):
        return self.__action_table.get(name)

# extend cgi.FieldStorage with the special fdsnws POST format
# FIXME: this must be made more generic, because not all services use the fdsnws format.
# TODO: move NSLC/start/end parsing (incl. GET parameters!) to a separate utility module?
class WIFieldStorage(cgi.FieldStorage):
    def __init__(self, **kwargs):
        if kwargs['environ']['REQUEST_METHOD'] == 'GET':
            self.body = None
            cgi.FieldStorage.__init__(self, **kwargs)
            return

        self.body = []
        cgi.FieldStorage.__init__(self)

        fp = io.BytesIO(kwargs['fp'].read(cgi.maxlen))

        if kwargs['fp'].read(1):
            raise ValueError("maximum request size exceeded")

        inHeader = True

        for line in fp:
            if inHeader and ('=' not in line):
                inHeader = False

            try:
                if inHeader:
                    (key, value) = line.split('=')

                    self.list.append(cgi.MiniFieldStorage(key.strip(), value.strip()))

                else:
                    (net, sta, loc, cha, start, endt) = line.split()

                    net = net.upper()
                    sta = sta.upper()
                    loc = loc.upper()
                    cha = cha.upper()

                    for code in (net, sta, loc, cha):
                        if not re.match('^[A-Z0-9\*\?\-,]+$', code):
                            raise ValueError, "invalid NSLC code: " + code

                    start = None if start in ("''", '""') else dateutil.parser.parse(start)
                    endt = None if endt in ("''", '""') else dateutil.parser.parse(endt)

                    self.body.append((net, sta, loc, cha, start, endt))

            except ValueError as e:
                logs.error(str(e))
                raise ValueError("wrong format detected while processing: %s" % line)

wi = WebInterface(__name__)

def application(environ, start_response):
    """Main WSGI handler that processes client requests and calls
    the proper functions.

    Begun by Javier Quinteros <javier@gfz-potsdam.de>,
    GEOFON team, June 2013

    """

    fname = environ['PATH_INFO']
    logs.debug("fname: %s" % (fname))

    action = wi.getAction(fname)
    logs.debug("action: %s" % str(action))

    # Among others, this will filter wrong function names,
    # but also the favicon.ico request, for instance.
    if action is None:
       status = "404 Not Found"
       return _send_plain_response(status, "Error! " + status, start_response)

    try:
        form = WIFieldStorage(fp=environ['wsgi.input'], environ=environ)

    except ValueError as e:
        logs.error("400 Bad Request: " + str(e))
        return _send_plain_response("400 Bad Request", str(e), start_response)

    logs.debug("parameters: %s" % str(["%s=%s" % (f.name, f.value) for f in form.list]))

    try:
        response = action(environ, form)
        if isinstance(response, WIResponse):
            return response.func("200 OK", response.body, start_response)

        elif response is None:
            logs.debug("204 No Content")
            return _send_plain_response("204 No Content", "", start_response)

    except PlsRedirect as redir:
        return _redirect_page(redir.url, start_response)

    except WIContentError as e:
        logs.notice(e.status)
        return _send_plain_response(e.status, "", start_response)

    except WIError as e:
        logs.error(e.status + ": " + e.body)
        return _send_plain_response(e.status, e.body, start_response)

    logs.error("invalid response from %s: %s" % (action, repr(response)))
    return _send_plain_response("500 Internal Server Error", "", start_response)

