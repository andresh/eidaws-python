#!/usr/bin/env python

import sys
import time
import shutil
import optparse
import urlparse
import urllib
import urllib2
import cookielib
import tempfile
import threading
import Queue
import socket
import csv
import io

VERSION = "1.2 (2015.349)"

GET_PARAMS = set(('net', 'network',
                  'sta', 'station',
                  'loc', 'location',
                  'cha', 'channel',
                  'start', 'starttime',
                  'end', 'endtime',
                  'service',
                  'alternative'))

POST_PARAMS = set(('service',
                   'alternative'))

class Error(Exception):
    pass

class TargetURL(object):
    def __init__(self, url, qp):
        self.__scheme = url.scheme
        self.__netloc = url.netloc
        self.__path = url.path.rstrip('query').rstrip('/')
        self.__qp = dict(qp)

    def auth(self):
        path = self.__path + '/auth'
        return urlparse.urlunparse(('https', self.__netloc, path, '', '', ''))

    def post(self):
        path = self.__path + '/query'
        return urlparse.urlunparse((self.__scheme, self.__netloc, path, '', '', ''))

    def post_qa(self):
        path = self.__path + '/queryauth'
        return urlparse.urlunparse((self.__scheme, self.__netloc, path, '', '', ''))

    def post_params(self):
        return self.__qp.items()

class RoutingURL(object):
    def __init__(self, url, qp):
        self.__scheme = url.scheme
        self.__netloc = url.netloc
        self.__path = url.path.rstrip('query').rstrip('/')
        self.__qp = dict(qp)

    def get(self):
        path = self.__path + '/query'
        qp = [(p, v) for (p, v) in self.__qp.items() if p in GET_PARAMS]
        qp.append(('format', 'post'))
        query = urllib.urlencode(qp)
        return urlparse.urlunparse((self.__scheme, self.__netloc, path, '', query, ''))

    def post(self):
        path = self.__path + '/query'
        return urlparse.urlunparse((self.__scheme, self.__netloc, path, '', '', ''))

    def post_params(self):
        qp = [(p, v) for (p, v) in self.__qp.items() if p in POST_PARAMS]
        qp.append(('format', 'post'))
        return qp

    def target_params(self):
        return [(p, v) for (p, v) in self.__qp.items() if p not in GET_PARAMS]

msglock = threading.Lock()

def msg(verb, s):
    if verb:
        with msglock: print >>sys.stderr, s

def retry(urlopen, url, data, timeout, count, wait, verb):
    n = 0

    while True:
        if n >= count:
            return urlopen(url, data, timeout)

        try:
            n += 1

            fd = urlopen(url, data, timeout)

            if fd.getcode() == 200 or fd.getcode() == 204:
                return fd

            msg(verb, "retrying %s (%d) after %d seconds due to HTTP status code %d" % (url, n, wait, fd.getcode()))
            time.sleep(wait)

        except urllib2.HTTPError as e:
            if e.code >= 400 and e.code < 500:
                raise

            msg(verb, "retrying %s (%d) after %d seconds due to %s" % (url, n, wait, str(e)))
            time.sleep(wait)

        except (urllib2.URLError, socket.error) as e:
            msg(verb, "retrying %s (%d) after %d seconds due to %s" % (url, n, wait, str(e)))
            time.sleep(wait)

def fetch(url, cred, authdata, postlines, dest, timeout, retry_count, retry_wait, finished, lock, verb):
    try:
        url_handlers = []

        if cred and url.qa() in cred: # use static credentials
            query_url = url.post_qa()
            mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
            url_handlers.append(urllib2.HTTPDigestAuthHandler(mgr))
            (user, passwd) = cred[query_url]
            mgr.add_password(None, query_url, user, passwd)

        elif authdata: # use the pgp-based auth method
            auth_url = url.auth()
            query_url = url.post_qa()
            mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
            url_handlers.append(urllib2.HTTPDigestAuthHandler(mgr))

            msg(verb, "authenticating at %s" % auth_url)

            try:
                fd = retry(urllib2.urlopen, auth_url, authdata, timeout, retry_count, retry_wait, verb)

                try:
                    if fd.getcode() == 200:
                        up = fd.read()

                        try:
                            (user, passwd) = up.split(':')
                            mgr.add_password(None, query_url, user, passwd)

                        except ValueError:
                            msg(True, "invalid auth response: %s" % up)
                            return

                        msg(verb, "authentication at %s successful" % auth_url)

                    else:
                        msg(True, "authentication at %s failed with HTTP status code %d" % (auth_url, fd.getcode()))

                finally:
                    fd.close()

            except (urllib2.URLError, socket.error) as e:
                msg(True, "authentication at %s failed: %s" % (auth_url, str(e)))
                return

        else: # fetch data anonymously
            query_url = url.post()

        opener = urllib2.build_opener(*url_handlers)

        i = 0
        n = len(postlines)

        while i < len(postlines):
            if n == len(postlines):
                msg(verb, "getting data from %s" % query_url)

            else:
                msg(verb, "getting data from %s (%d%%..%d%%)" % (query_url, 100*i/len(postlines), min(100, 100*(i+n)/len(postlines))))

            postdata = ''.join((p + '=' + v + '\n') for (p, v) in url.post_params()) + ''.join(postlines[i:i+n])

            try:
                fd = retry(opener.open, query_url, postdata, timeout, retry_count, retry_wait, verb)

                try:
                    if fd.getcode() == 204:
                        msg(verb, "received no data from %s" % query_url)

                    elif fd.getcode() != 200:
                        msg(True, "getting data from %s failed with HTTP status code %d" % (query_url, fd.getcode()))
                        break

                    else:
                        size = 0

                        content_type = fd.info().getheader('Content-Type')

                        if content_type == "application/vnd.fdsn.mseed":
                            while True:
                                buf = fd.read(4096)
                                if not buf: break
                                with lock: dest.write(buf)
                                size += len(buf)

                            msg(verb, "got %d bytes from %s" % (size, query_url))

                        elif content_type == "text/plain":
                            while True:
                                buf = fd.readline()
                                if not buf: break
                                with lock: dest.write(buf)
                                size += len(buf)

                        elif content_type == "application/xml":
                            with tempfile.TemporaryFile() as tmpfd:
                                while True:
                                    buf = fd.read(4096)
                                    if not buf: break
                                    tmpfd.write(buf)
                                    size += len(buf)

                                msg(verb, "got %d bytes from %s" % (size, query_url))

                                tmpfd.seek(0)
                                with lock: shutil.copyfileobj(tmpfd, dest)

                        else:
                            msg(True, "getting data from %s failed: unsupported content type '%s'" % (query_url, content_type))
                            break

                    i += n

                finally:
                    fd.close()

            except urllib2.HTTPError as e:
                if e.code == 413 and n > 1:
                    msg(verb, "request too large for %s, splitting" % query_url)
                    n = -(n//-2)

                else:
                    msg(True, "getting data from %s failed: %s" % (query_url, str(e)))
                    break

            except (urllib2.URLError, socket.error) as e:
                msg(True, "getting data from %s failed: %s" % (query_url, str(e)))
                break

    finally:
        finished.put(threading.current_thread())

def route(url, cred, authdata, postdata, dest, timeout, retry_count, retry_wait, maxthreads, verb):
    threads = []
    running = 0
    finished = Queue.Queue()
    lock = threading.Lock()

    if postdata:
        query_url = url.post()
        postdata = ''.join((p + '=' + v + '\n') for (p, v) in url.post_params()) + postdata

    else:
        query_url = url.get()

    msg(verb, "getting routes from %s" % query_url)

    try:
        fd = retry(urllib2.urlopen, query_url, postdata, timeout, retry_count, retry_wait, verb)

        try:
            if fd.getcode() == 204:
                raise Error("received no routes from %s" % query_url)

            elif fd.getcode() != 200:
                raise Error("getting routes from %s failed with HTTP status code %d" % (query_url, fd.getcode()))

            else:
                urlline = None
                postlines = []

                while True:
                    line = fd.readline()

                    if not urlline:
                        urlline = line.strip()

                    elif not line.strip():
                        if postlines:
                            target_url = TargetURL(urlparse.urlparse(urlline), url.target_params())
                            threads.append(threading.Thread(target=fetch, args=(target_url, cred, authdata,
                                postlines, dest, timeout, retry_count, retry_wait, finished, lock, verb)))

                        urlline = None
                        postlines = []

                        if not line:
                            break

                    else:
                        postlines.append(line)

        finally:
            fd.close()

    except (urllib2.URLError, socket.error) as e:
        raise Error("getting routes from %s failed: %s" % (query_url, str(e)))

    for t in threads:
        if running >= maxthreads:
            thr = finished.get(True)
            thr.join()
            running -= 1

        t.start()
        running += 1

    while running:
        thr = finished.get(True)
        thr.join()
        running -= 1

def main():
    qp = {}
    wfmeta = {}

    def add_qp(option, opt_str, value, parser):
        if option.dest == 'query':
            try:
                (p, v) = value.split('=', 1)
                qp[p] = v

            except ValueError as e:
                raise optparse.OptionValueError("%s expects parameter=value" % opt_str)

        else:
            qp[option.dest] = value

    def add_wfmeta(option, opt_str, value, parser):
        try:
            (p, v) = value.split('=', 1)
            wfmeta[p] = v

        except ValueError as e:
            raise optparse.OptionValueError("%s expects parameter=value" % opt_str)

    parser = optparse.OptionParser(usage="Usage: %prog [-h|--help] [OPTIONS] -o file", version="%prog v" + VERSION)

    parser.set_defaults(url = "http://geofon.gfz-potsdam.de/eidaws/routing/1/",
                        timeout = 600,
                        retries = 10,
                        retry_wait = 60,
                        threads = 5)

    parser.add_option("-v", "--verbose", action="store_true", default=False,
        help="verbose mode")

    parser.add_option("-u", "--url", type="string",
        help="URL of routing service (default %default)")

    parser.add_option("-y", "--service", type="string", action="callback", callback=add_qp,
        help="target service (default dataselect)")

    parser.add_option("-N", "--network", type="string", action="callback", callback=add_qp,
        help="network code or pattern")

    parser.add_option("-S", "--station", type="string", action="callback", callback=add_qp,
        help="station code or pattern")

    parser.add_option("-L", "--location", type="string", action="callback", callback=add_qp,
        help="location code or pattern")

    parser.add_option("-C", "--channel", type="string", action="callback", callback=add_qp,
        help="channel code or pattern")

    parser.add_option("-s", "--starttime", type="string", action="callback", callback=add_qp,
        help="start time")

    parser.add_option("-e", "--endtime", type="string", action="callback", callback=add_qp,
        help="end time")

    parser.add_option("-q", "--query", type="string", action="callback", callback=add_qp,
        help="additional query parameter", metavar="PARAMETER=VALUE")

    parser.add_option("-m", "--wf-meta", type="string", action="callback", callback=add_wfmeta,
        help="wfmetadataselect parameter", metavar="PARAMETER=VALUE")

    parser.add_option("-t", "--timeout", type="int",
        help="request timeout in seconds (default %default)")

    parser.add_option("-r", "--retries", type="int",
        help="number of retries (default %default)")

    parser.add_option("-w", "--retry-wait", type="int",
        help="seconds to wait before each retry (default %default)")

    parser.add_option("-n", "--threads", type="int",
        help="maximum number of download threads (default %default)")

    parser.add_option("-c", "--credentials-file", type="string",
        help="URL,user,password (CSV format) for queryauth")

    parser.add_option("-a", "--auth-file", type="string",
        help="auth file (secure mode)")

    parser.add_option("-p", "--post-file", type="string",
        help="data file for POST request")

    parser.add_option("-o", "--output-file", type="string",
        help="file where downloaded data is written")

    (options, args) = parser.parse_args()

    if args or not options.output_file:
        parser.print_usage()
        return 1

    try:
        if options.credentials_file:
            cred = {}

            try:
                for (url, user, passwd) in csv.reader(open(options.credentials_file)):
                    cred[url] = (user, passwd)

            except (ValueError, csv.Error):
                raise Error("error parsing %s" % options.credentials_file)

        else:
            cred = None

        postdata = open(options.post_file).read() if options.post_file else None

        if wfmeta:
            for p in 'network', 'station', 'location', 'channel', 'starttime', 'endtime':
                wfmeta[p] = qp[p]

            wfmeta['service'] = 'wfmetadataselect'
            wfmeta['collapsed'] = 'true'
            wfmeta['format'] = 'post'

            url = RoutingURL(urlparse.urlparse(options.url), wfmeta)
            dest = io.BytesIO()

            route(url, None, None, postdata, dest, options.timeout, options.retries, options.retry_wait,
                options.threads, options.verbose)

            postdata = dest.getvalue()

            if not postdata:
                msg(True, "no data available with requested quality")
                return 1

        url = RoutingURL(urlparse.urlparse(options.url), qp)
        authdata = open(options.auth_file).read() if options.auth_file else None
        dest = open(options.output_file, 'w')

        route(url, cred, authdata, postdata, dest, options.timeout, options.retries, options.retry_wait,
            options.threads, options.verbose)

    except (IOError, Error) as e:
        msg(True, str(e))
        return 1

    return 0

if __name__ == "__main__":
    sys.exit(main())

